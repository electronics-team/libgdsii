Source: libgdsii
Maintainer: Debian Electronics Team <pkg-electronics-devel@lists.alioth.debian.org>
Uploaders: Ruben Undheim <ruben.undheim@gmail.com>
Section: libs
Priority: optional
Build-Depends: debhelper-compat (= 12),
               txt2man
Standards-Version: 4.4.0
Vcs-Browser: https://salsa.debian.org/electronics-team/libgdsii
Vcs-Git: https://salsa.debian.org/electronics-team/libgdsii.git
Homepage: https://github.com/HomerReid/libGDSII

Package: libgdsii0
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Library for GDSII handling
 This is a C++ library for working with GDSII binary data files, intended
 primarily for use with the computational electromagnetism codes scuff-em and
 meep but sufficiently general-purpose to allow other uses as well.

Package: libgdsii-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: ${misc:Depends},
         libgdsii0 (= ${binary:Version})
Description: Library for GDSII handling (development files)
 This is a C++ library for working with GDSII binary data files, intended
 primarily for use with the computational electromagnetism codes scuff-em and
 meep but sufficiently general-purpose to allow other uses as well.
 .
 This package contains the development files for the library.

Package: gdsiiconvert
Architecture: any
Section: utils
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libgdsii0 (= ${binary:Version})
Description: Convert GDSII geometries and report geometry statistics
 The command line tool can be used for reporting statistics on GDSII geometries
 and export them to other file formats, notably including the GMSH geometry
 format.
 .
 It is the "example application" for libGDSII which is a C++ library for
 working with GDSII binary data files.
 .
 This package also contains example GDSII files.
