NAME
  GDSIIConvert - Convert GDSII geometries and report geometry statistics

SYNOPSIS
  GDSIIConvert [options]

DESCRIPTION
  This is a command-line executable code for reporting statistics on GDSII geometries and
  exporting them to other file formats, notably including the [GMSH](http://gmsh.info) geometry format.

  For converting to GMSH format:

    GDSIIConvert --GMSH myexample.gds


OPTIONS
 ** Output formats: **
   --raw              raw dump of file data records
   --analyze          detailed listing of hierarchical structure
   --GMSH             Export GMSH geometry to FileBase.geo (text strings to FileBase.pp)
   --scuff-rf         Write .port file defining RF ports for scuff-RF (implies --gmsh)

 ** Other flags: **
   --MetalLayer 12      define layer 12 as a metal layer (may be specified multiple times)
   --LengthUnit <mm>    set output length unit in mm (default = 1)
   --FileBase <basename>  set base name for output files
   --verbose            produce more output
   --SeparateLayers     write separate output files for objects on each layer





AUTHOR
  This manual page was written by Ruben Undheim <ruben.undheim@gmail.com> for the Debian project (and may be used by others).



